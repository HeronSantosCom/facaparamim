function getParamFromURI(name) {
    var value = decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
    if ($.isset(value)) {
        if (value != "null") {
            return value;
        }
    }
    return false;
}

$(function() {

    jQuery.support.placeholder = false;
    test = document.createElement('input');
    if ('placeholder' in test)
        jQuery.support.placeholder = true;
    if (!$.support.placeholder) {
        $('.field').find('label').show();
    }

    // verifica se o campo é nulo
    $.isset = function(obj) {
        return (obj != undefined);
    };

    // verifica se o campo é nulo
    $.isnull = function(obj) {
        if ($.isset(obj)) {
            if (obj.length > 0 && obj != false) {
                return false;
            }
        }
        return true;
    };

    $.regex = function(valor, patt) {
        var result = patt.exec(valor);
        if (result == null) {
            return false;
        }
        return true;
    }

    $.number_format = function(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    $.string2float = function(value, input, digits) {
//        if (input != undefined && input == true) {
        if ($.isnull(input) == false) {
            value = value.replace(/\./g, "");
        }
        value = value.replace(/\,/g, ".");
        value = $.number_format(value, (digits != undefined ? digits : 5), ".", "");
        value = parseFloat(value);
        return value;
    }

    $.changeSelect = function(select, optgroup, option) {
        $("." + select.attr("id")).remove();
        select.parent().append('<input type="hidden" class="' + select.attr("id") + '" name="' + optgroup.attr("id") + '" value="' + option.val() + '">');
    }

    $.enableBackToTop = function() {
        var backToTop = $('<a>', {
            id: 'back-to-top',
            href: '#top'
        });
        var icon = $('<i>', {
            'class': 'icon-chevron-up'
        });
        backToTop.appendTo('body');
        icon.appendTo(backToTop);
        backToTop.hide();
        $(window).scroll(function() {
            if ($(this).scrollTop() > 150) {
                backToTop.fadeIn();
            } else {
                backToTop.fadeOut();
            }
        });
        backToTop.click(function(e) {
            e.preventDefault();
            $('body, html').animate({
                scrollTop: 0
            }, 600);
        });
    }

    $.redirect = function(url) {
        window.location = url;
        return false;
    };

    $.url = function(action) {
        return "index.html" + ($.isnull(action) == false ? "?a=" + action : "");
    };

    $.action = function(action, id, page) {
        if ($.isset(action) == false || action === false) {
            action = getParamFromURI("a");
        }
        id = ($.isset(id) ? id : getParamFromURI("id"));
        page = ($.isset(page) ? page : getParamFromURI("p"));
        return $.redirect(
                $.url(action)
                + ($.isnull(id) == false ? "&id=" + id : "")
                + ($.isnull(page) == false ? "&p=" + page : false)
                );
    };

    $.enableBackToTop();

    $(".msgbox").each(function() {
        $(this).modal('show');
    });


    $("input[type='checkbox']").each(function() {
        if ($(this).attr("rel") == $(this).val()) {
            $(this).click();
        }

    });

    $("input[type='radio']").each(function() {
        if ($(this).attr("rel") == $(this).val()) {
            $(this).click();
        }
    });

    $("select").each(function() {
        var select = $(this);
        var optgroup = $(this).find("optgroup");
//        if (optgroup.length) {
        if ($.isnull(optgroup) == false) {
            select.change(function() {
                var option = select.find("option:selected");
                $.changeSelect(select, option.parent(), option);
            });
            var selected = false;
            optgroup.each(function() {
                var rel = $(this).attr("rel");
//                if (rel.length && !selected) {
                if ($.isnull(rel) == false && !selected) {
                    selected = $(this).find("option[value='" + rel + "']").eq(0);
                    selected.attr("selected", "selected");
                    $.changeSelect(select, $(this), selected);
                }
            });
            if (!selected) {
                optgroup = optgroup.eq(0);
                selected = optgroup.find("option").eq(0);
                selected.attr("selected", "selected");
                $.changeSelect(select, optgroup, selected);
            }
        } else {
            var rel = $(this).attr("rel");
            if ($.isnull(rel) == false) {
                $(this).val(rel);
            }
        }
    });

    $(".openbox").click(function(ev) {
        ev.preventDefault();
        var target = $(this).attr("href");
        $("body").append("");
        // load the url and show modal on success
        $("#openbox .modal-body").load(target, function() {
            $("#openbox").modal("show");
        });
    });

    $(".troca-pagina").click(function() {
        $.action(false, false, $(this).attr("rel"));
    });

    $("form.validatable").validationEngine({
        promptPosition: "topLeft"
    });

    $.fn.validate_modal = function() {
        var obj = this;
        $(obj).validationEngine({
            promptPosition: "topLeft"
                    , ajaxFormValidation: true
                    , ajaxFormValidationMethod: 'post'
                    , onAjaxFormComplete: function(status, form, json, options) {
                switch (json.retorno) {
                    case 'success':
                        $(obj).parent().parent().modal('hide');
                        Growl.success({title: json.titulo, text: json.mensagem});
                        break;
                    case 'warn':
                        Growl.warn({title: json.titulo, text: json.mensagem});
                        break;
                    case 'error':
                        Growl.error({title: json.titulo, text: json.mensagem});
                        break;
                    case 'info':
                        Growl.info({title: json.titulo, text: json.mensagem});
                        break;
                }
            }
        });
    };

    $("select.uniform, input:file").uniform();
    $(".chzn-select").select2();
    $(".icheck").iCheck({checkboxClass: "icheckbox_flat-aero", radioClass: "iradio_flat-aero"});

    $(".allcheck").on("ifChecked", function() {
        $(".checkchild").iCheck('check');
    });
    $(".allcheck").on("ifUnchecked", function() {
        $(".checkchild").iCheck('uncheck');
    });
    
 

});