<?php

class auth extends \main {

    public static function cookie($dao = false) {
        if ($dao) {
            return setcookie("auth", base64_encode(serialize($dao)), time() + 60 * 60 * 24 * 100, "/");
        }
        return (!empty($_COOKIE["auth"]) ? unserialize(base64_decode($_COOKIE["auth"])) : false);
    }

    public static function session($dao = false) {
        if ($dao) {
            return $_SESSION["auth"][] = $dao;
        }
        return self::locked();
    }

    public static function reset() {
        if (!empty($_SESSION["auth"])) {
            $session = $_SESSION["auth"];
            if (is_array($session)) {
                $remove = array_pop($session);
                return $_SESSION["auth"] = $session;
            }
        }
        setcookie("auth", false, time() + 60 * 60 * 24 * 100, "/");
        $_SESSION["auth"] = false;
        return true;
    }

    public static function locked() {
        if (!empty($_SESSION["auth"])) {
            \knife::dump($_SESSION["auth"]);
            $session = $_SESSION["auth"];
            if (is_array($session)) {
                $instance = array_pop($session);
                return $instance;
            }
        }
        return false;
    }

}