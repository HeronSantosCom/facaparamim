<?php

class main extends \app {

    public function __construct() {
        $this->discover();
    }

    private static function autoclass($open) {
        return class_exists($open);
    }

    private function autoloader($open, $function = false) {
        if ($open) {
            $open = str_replace("/", '\\', $open);
            if ($this->autoclass($open)) {
                $instance = new $open();
                if ($instance && $function) {
                    return $instance->$function();
                }
                return true;
            }
            $open = explode('\\', $open);
            $function = array_pop($open);
            return $this->autoloader(join("/", $open), $function);
        }
        return false;
    }

    private function discover() {
        $open = "index";
        $xhr = false;
        if (!empty($_SERVER["REDIRECT_URL"])) {
            $open = explode("/", str_replace(".html", "", $_SERVER["REDIRECT_URL"]));
            array_shift($open);
            if ($open[0] === "xhr") {
                array_shift($open);
                $xhr = true;
            }
            $open = strtolower(join("/", $open));
        }
        return $this->open($open, $xhr);
    }

    private function open($open = "index", $xhr = false) {
        $response = $this->autoloader("ctrl/{$open}");
        if (ajax || $xhr) {
            return $this->xhr($response);
        }
    }

    private function xhr($response) {
        echo json_encode(array("response" => (!empty($response)), "data" => $response));
    }

    public static function shadow($value) {
        $hash = base64_decode(strtr($value, '-_.', '+/='));
        if (substr($hash, 0, strlen(self::certificate)) === self::certificate) {
            return unserialize(substr($hash, strlen(self::certificate)));
        }
        return strtr(base64_encode(self::certificate . serialize($value)), '+/=', '-_.');
    }

}