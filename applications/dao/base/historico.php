<?php

namespace dao\base {

    class historico extends \dao\dao {

        private static function dao() {
            $dao = new \mysqlsearch();
            $dao->table("historico"); // 1
            $dao->column("*", false);
            return $dao;
        }

        private static function result($dao, $idx = false) {
            if ($dao) {
                foreach ($dao as $key => $row) {
                    $dao[$key] = self::hook($row);
                }
            }
            return ($idx && !empty($dao[($idx - 1)]) ? $dao[($idx - 1)] : $dao);
        }

        private static function hook($row) {
            if ($row) {
                $row["id"] = parent::shadow($row["id"]);
            }
            return $row;
        }

        public static function pegar($id) {
            $db = self::dao();
            $db->match("id", parent::shadow($id));
            return self::result($db->go(false, false), 1);
        }

        public static function listar($filtro = false, $ordem = false) {
            $db = self::dao();
            $db = parent::filter($db, $filtro);
            $db = parent::order($db, $ordem);
            return self::result($db->go(false, false));
        }

        public static function pesquisar($filtro = false, $ordem = false, $pagina = 1, $limite = 30) {
            $db = self::dao();
            $db = parent::filter($db, $filtro);
            $db = parent::order($db, $ordem);
            $dao = parent::pagination($db, $pagina, $limite);
            if ($dao) {
                $dao['resultado'] = self::result($dao['resultado']);
            }
            return $dao;
        }

        public static function cadastrar() {
            
        }

        public static function atualizar() {
            
        }

        public static function remover() {
            
        }

    }

}