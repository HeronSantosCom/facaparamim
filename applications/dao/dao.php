<?php

namespace dao {

    class dao extends \main {

        /**
         * Prepara o filtro da consulta de acordo com os parametros passados
         * @param object $db
         * @param array $filtro($key => $value, ...)
         * @return object $db
         */
        protected static function filter($db, $filtro) {
            if (is_array($filtro)) {
                foreach ($filtro as $key => $value) {
                    switch ($key) {
                        case "id":
                            $db->match("id", parent::shadow($value));
                            break;
                        default:
                            $db->like($key, "%{$value}%");
                            break;
                    }
                }
            }
            return $db;
        }

        /**
         * Prepara a paginação da consulta de acordo com os parametros passados
         * @param object $db
         * @param array $ordenacao($order => $orderby, $order, ...)
         * @return object $db
         */
        protected static function order($db, $ordenacao) {
            if (is_array($ordenacao)) {
                foreach ($ordenacao as $order => $orderby) {
                    if ($order > 0) {
                        $db->order($order, $orderby);
                    } else {
                        $db->order($orderby);
                    }
                }
            }
            return $db;
        }

        /**
         * Prepara a paginação da consulta de acordo com os parametros passados
         * @param object $db
         * @param integer $pagina
         * @param integer $limite
         * @return array $dao(resultado, paginas, exibindo, total)
         */
        protected static function pagination($db, $pagina = 1, $limite = 30) {
            $inicial = ($pagina * $limite) - $limite;
            $db->limit($limite, $inicial);
            $dao = $db->go();
            return ($dao ? array('resultado' => $dao[0], 'paginas' => ceil($dao[1]['founds'] > 0 ? $dao[1]['founds'] / $limite : 1), 'exibindo' => $dao[1]['displayed'], 'total' => $dao[1]['founds']) : false);
        }

    }

}