<?php

namespace ctrl {

    class entrar extends \auth {

        public function __construct() {
            $this->login();
            if (parent::locked()) {
                \knife::redirect("index.html");
            }
            \knife::dump(\dao\base\historico::pesquisar());
            $dao = \dao\base\historico::listar();
            \knife::dump($dao);
            if ($dao) {
                \knife::dump(\dao\base\historico::pegar($dao[0]['id']));
            }
        }

        private function login() {
            $this->extract(parent::cookie());
            $this->extract($_POST);
            if ($this->usuario && $this->senha) {
                if (parent::session($_POST)) {
                    return true;
//                    \knife::redirect("index.html");
                }
//                if (self::reboot(false, $this->usuario, $this->senha, $this->relembrar)) {
//                    return true;
//                }
//                $this->msgbox("O nome de usuário ou a senha inserida está(ão) incorreto(s).");
            }
            return false;
        }

    }

}
