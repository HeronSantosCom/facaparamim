<?php

namespace ctrl {

    class index extends \auth {

        public function __construct() {
            if (isset($_GET["sair"]) || isset($_GET["reset"])) {
                $this->sair();
            }
            $this->acesso();
        }

        public function acesso() {
            if (!self::locked()) {
                \knife::redirect("entrar.html");
            }
        }

        public function sair() {
            return parent::reset();
        }

    }

}
