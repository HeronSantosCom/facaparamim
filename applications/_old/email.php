<?php

class email {

    /**
     * Envia para o Cliente
     * @param string $email
     * @param string $titulo
     * @param array $mensagem
     * @return boolean
     */
    private static function _usuario($email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = "Lembre-se, em caso de dúvidas ou sugestões entre em contato conosco através dos nossos canais de atendimento!";
        $mensagem[] = "";
        $mensagem[] = "Insign Digital";
        $mensagem[] = "http://www.facaparamim.com/";
        $mensagem[] = "";
        $mensagem[] = "---";
        $mensagem[] = "Notificação gerada e enviada automaticamente.";
        $mensagem[] = date("r") . " / " . getmypid() . (!empty($_SERVER["REMOTE_ADDR"]) ? " / " . $_SERVER["REMOTE_ADDR"] : false );
        $headers = "From: Insign Digital <contato@facaparamim.com>\n";
        $headers .= "Reply-To: Insign Digital <contato@facaparamim.com>\n";
        return knife::mail_utf8($email, '[Insign Digital] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

    /**
     * Envia para o Gestor
     * @param string $nome
     * @param string $email
     * @param string $titulo
     * @param array $mensagem
     * @return boolean
     */
    private static function _gestor($nome, $email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = "Insign Digital";
        $mensagem[] = "http://www.facaparamim.com/";
        $mensagem[] = "";
        $mensagem[] = "---";
        $mensagem[] = "Notificação gerada e enviada automaticamente.";
        $mensagem[] = date("r") . " / " . getmypid() . (!empty($_SERVER["REMOTE_ADDR"]) ? " / " . $_SERVER["REMOTE_ADDR"] : false );
        $headers = "From: " . $nome . " <" . $email . ">\n";
        $headers .= "Reply-To: " . $nome . " <" . $email . ">\n";
        return knife::mail_utf8("contato@facaparamim.com", '[Faça Para Mim] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

    /**
     * Notifica Redefição de Senha do Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $senha
     * @return boolean
     */
    public static function usuario_redefinir_senha($nome, $email, $senha) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Como solicitado, estamos enviando sua nova senha de acesso!";
        $mensagem[] = "Por segurança, anote em um local seguro.";
        $mensagem[] = "";
        $mensagem[] = "\tSua nova senha é: {$senha}";
        $mensagem[] = "\tEndereço de acesso: http://www.facaparamim.com/";
        $mensagem[] = "";
        $mensagem[] = "Esta é uma senha gerada aleatóriamente, por favor, altere se assim desejar!";
        return self::_usuario($email, "Sua senha foi redefinida", $mensagem);
    }

}