<?php

class mod_perfil_acesso extends main {

    public function __construct() {
        $this->extract($_GET);
    }

    public function index() {
        $this->extract(self::filtro("perfil_acesso/index"));
        $tabela = dao_perfil_acesso::tabela(array("nome" => $this->nome), $this->p);
        if ($tabela) {
            $this->perfil_acessos = $tabela["retorno"];
            $this->limite = $tabela["limite"];
            $this->registros = $tabela["total"];
            $this->paginas = self::paginacao($this->p, $tabela["paginas"]);
        }
    }

    public function cadastrar() {
        if (isset($_POST['submit'])) {
            $this->extract($_POST);
            $action = dao_perfil_acesso::cadastrar($this->nome, $this->nivel);
            if ($action) {
                $this->session_growl("Cadastro de perfil de acesso", "Perfil de acesso cadastrado com sucesso!");
                knife::redirect("index.html?a=perfil_acesso/index");
            }
        }
        $this->sistemas = dao_sistema::listar();
    }

    public function alterar() {
        if ($this->id) {
           if (isset($_POST['submit'])) {
                $this->extract($_POST);
                $action = dao_perfil_acesso::atualizar($this->id, $this->nome, $this->nivel);
                if ($action) {
                    $this->session_growl("Alteração de perfil de acesso", "Perfil de acesso alterado com sucesso!");
                    knife::redirect("index.html?a=perfil_acesso/index");
                }
                return;
            }
            $this->abrir($this->id);
        } else {
            knife::redirect("index.html?a=perfil_acesso/cadastrar");
        }
    }

    /**
     * Remove Item
     * @param integer $id
     */
    public function remover() {
        $this->session_growl("Remover sistema", "Nenhum perfil de acesso selecionado!", 'error');
        if ($_POST) {
            if (!empty($_POST['id'])) {
                $removidos = true;
                foreach ($_POST['id'] as $id) {
                    if (!dao_perfil_acesso::remover($id)) {
                        $removidos = false;
                    }
                }
                $this->session_growl("Remover perfil de acesso", ($removidos ? "Perfil de acesso(s) removido com sucesso!" : "Alguns perfil de acesso(s) não foram removidos!"), ($removidos ? 'success' : 'warn'));
            }
        }
        knife::redirect("index.html?a=perfil_acesso/index");
    }

    /**
     * Carrega Item
     * @param integer $id
     * @return boolean
     */
    private function abrir($id) {
        $dao = (dao_perfil_acesso::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

}