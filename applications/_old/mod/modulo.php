<?php

class mod_modulo extends main {

    public function __construct() {
        $this->extract($_GET);
    }

    public function index() {
        $this->extract(self::filtro("modulo/index"));
        $tabela = dao_modulo::tabela(array("nome" => $this->nome), $this->p);
        if ($tabela) {
            $this->modulos = $tabela["retorno"];
            $this->limite = $tabela["limite"];
            $this->registros = $tabela["total"];
            $this->paginas = self::paginacao($this->p, $tabela["paginas"]);
        }
    }

    public function cadastrar() {
        if (isset($_POST['submit'])) {
            $this->extract($_POST);
            $action = dao_modulo::cadastrar($this->nome, $this->caminho, (isset($this->modal) ? 1 : 0), (isset($this->navbar) ? 1 : 0), (isset($this->navuser) ? 1 : 0), $this->modulo_id);
            if ($action) {
                $this->session_growl("Cadastro de modulo", "Módulo cadastrado com sucesso!");
                knife::redirect("index.html?a=modulo/index");
            }
        }
        $this->modulos = dao_modulo::listar();
    }

    public function alterar() {
        if ($this->id) {
            if (isset($_POST['submit'])) {
                $this->extract($_POST);
                $action = dao_modulo::atualizar($this->id, $this->nome, $this->caminho, (isset($this->modal) ? 1 : null), (isset($this->navbar) ? 1 : null), (isset($this->navuser) ? 1 : null), $this->modulo_id);
                if ($action) {
                    $this->session_growl("Alteração de modulo", "Módulo alterado com sucesso!");
                    knife::redirect("index.html?a=modulo/index");
                }
                return;
            }
            $this->abrir($this->id);
            $this->modulos = dao_modulo::listar();
        } else {
            knife::redirect("index.html?a=modulo/cadastrar");
        }
    }

    /**
     * Remove Item
     * @param integer $id
     */
    public function remover() {
        $this->session_growl("Remover plano", "Nenhum plano selecionado!", 'error');
        if ($_POST) {
            if (!empty($_POST['id'])) {
                $removidos = true;
                foreach ($_POST['id'] as $id) {
                    if (!dao_modulo::remover($id)) {
                        $removidos = false;
                    }
                }
                $this->session_growl("Remover modulo", ($removidos ? "Módulo(s) removido com sucesso!" : "Alguns modulo(s) não foram removidos!"), ($removidos ? 'success' : 'warn'));
            }
        }
        knife::redirect("index.html?a=modulo/index");
    }

    /**
     * Carrega Item
     * @param integer $id
     * @return boolean
     */
    private function abrir($id) {
        $dao = (dao_modulo::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

}