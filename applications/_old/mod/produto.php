<?php

class mod_produto extends main {

    public function __construct() {
        $this->extract($_GET);
    }

    public function index() {
        $this->extract(self::filtro("produto/index"));
        $tabela = dao_produto::tabela(array("nome" => $this->nome), $this->p);
        if ($tabela) {
            $this->produtos = $tabela["retorno"];
            $this->limite = $tabela["limite"];
            $this->registros = $tabela["total"];
            $this->paginas = self::paginacao($this->p, $tabela["paginas"]);
        }
    }

    public function cadastrar() {
        if (isset($_POST['submit'])) {
            $this->extract($_POST);
            $action = dao_produto::cadastrar($this->nome, $this->plano_id, $this->sistema_id);
            if ($action) {
                $this->session_growl("Cadastro de produto", "Plano cadastrado com sucesso!");
                knife::redirect("index.html?a=produto/index");
                return;
            }

            $this->session_growl("Falha ao cadastrar o produto", "Verifique os campos!", "error");
        }
        $this->planos = dao_plano::listar();
        $this->sistemas = dao_sistema::listar();
    }

    public function alterar() {
        if ($this->id) {
            if (isset($_POST['submit'])) {
                $this->extract($_POST);
                $action = dao_produto::atualizar($this->id, $this->nome, $this->plano_id, $this->sistema_id);
                if ($action) {
                    $this->session_growl("Alteração de produto", "Plano alterado com sucesso!");
                    knife::redirect("index.html?a=produto/index");
                }
            }
            $this->abrir($this->id);
            $this->planos = dao_plano::listar();
            $this->sistemas = dao_sistema::listar();
        } else {
            knife::redirect("index.html?a=produto/cadastrar");
        }
    }

    /**
     * Remove Item
     * @param integer $id
     */
    public function remover() {
        $this->session_growl("Remover produto", "Nenhum produto selecionado!", 'error');
        if ($_POST) {
            if (!empty($_POST['id'])) {
                $removidos = true;
                foreach ($_POST['id'] as $id) {
                    if (!dao_produto::remover($id)) {
                        $removidos = false;
                    }
                }
                $this->session_growl("Remover produto", ($removidos ? "Plano(s) removido com sucesso!" : "Alguns produto(s) não foram removidos!"), ($removidos ? 'success' : 'warn'));
            }
        }
        knife::redirect("index.html?a=produto/index");
    }

    /**
     * Carrega Item
     * @param integer $id
     * @return boolean
     */
    private function abrir($id) {
        $dao = (dao_produto::pegar($id));
        if ($dao) {
            $this->usuarios = dao_usuario_x_produto::listar(false, $id);
            $this->extract($dao);
            return true;
        }
        return false;
    }

}