<?php

class mod_usuario extends main {

    public function __construct() {
        $this->extract($_GET);
    }

    public function index() {
        $this->extract(self::filtro("usuario/index"));
        $tabela = dao_usuario::tabela(array("nome" => $this->nome), logon::meu_id(), $this->p);
        if ($tabela) {
            $this->usuarios = $tabela["retorno"];
            $this->limite = $tabela["limite"];
            $this->registros = $tabela["total"];
            $this->paginas = self::paginacao($this->p, $tabela["paginas"]);
        }
    }

    public function cadastrar() {
        if (isset($_POST['submit'])) {
            $this->extract($_POST);
            $action = dao_usuario::cadastrar($this->nome, $this->apelido, $this->email, $this->telefone, $this->senha);
            if ($action) {
                $this->session_growl("Cadastro de usuário", "Usuário cadastrado com sucesso!");
                knife::redirect("index.html?a=usuario/index");
            }
        }
    }

    public function alterar() {
        if ($this->id) {
            if (isset($_POST['submit'])) {
                $this->extract($_POST);
                $action = dao_usuario::atualizar($this->id, $this->nome, $this->apelido, $this->email, $this->telefone);
                if ($action) {
                    if ($this->id == logon::meu_id()) {
                        self::reboot(logon::meu_id(), false, false, (self::cookie() ? '1' : '0'));
                    }
                    $this->session_growl("Alteração de usuário", "Usuário alterado com sucesso!");
                    knife::redirect("index.html?a=usuario/index");
                }
            }
            $this->abrir($this->id);
        } else {
            knife::redirect("index.html?a=usuario/cadastrar");
        }
    }

    public function alterar_senha() {
        if ($this->id) {
            $usuario = dao_usuario::pegar($this->id);
            $this->apelido = $usuario['apelido'];
            $this->email = $usuario['email'];
        } else {
            $this->id = logon::meu_id();
            $this->apelido = logon::meu_apelido();
            $this->email = logon::meu_email();
        }

        if (isset($_POST['submit'])) {
            $this->extract($_POST);
            $retorno['titulo'] = "Alteração de senha!";
            $retorno['retorno'] = "error";
            $retorno['mensagem'] = "Não foi possível anterar sua senha.";
            if (dao_usuario::redefinir_senha($this->email, $this->senha)) {
                $retorno['retorno'] = "success";
                $retorno['mensagem'] = "Sua senha foi alterada com sucesso.";
                if ($this->id == logon::meu_id()) {
                    self::reboot(logon::meu_id(), false, false, (self::cookie() ? '1' : '0'));
                }
            }
            echo json_encode($retorno);
            return;
        }



        echo(knife::html("layout/main/usuario/alterar_senha.html"));
    }

    public function remover() {
        $this->session_growl("Remover usuário", "Nenhum usuário selecionado!", 'error');
        if ($_POST) {
            if (!empty($_POST['id'])) {
                $removidos = true;
                foreach ($_POST['id'] as $id) {
                    if (!dao_usuario::remover($id)) {
                        $removidos = false;
                    }
                }
                $this->session_growl("Remover usuário", ($removidos ? "Usuário(s) removido com sucesso!" : "Algum usuário(s) não foram removidos!"), ($removidos ? 'success' : 'warn'));
            }
        }

        knife::redirect("index.html?a=usuario/index");
    }

    /**
     * Carrega Item
     * @param integer $id
     * @return boolean
     */
    private function abrir($id) {
        $dao = (dao_usuario::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

}