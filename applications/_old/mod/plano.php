<?php

class mod_plano extends main {

    public function __construct() {
        $this->extract($_GET);
    }

    public function index() {
        $this->extract(self::filtro("plano/index"));
        $tabela = dao_plano::tabela(array("nome" => $this->nome), $this->p);
        if ($tabela) {
            $this->planos = $tabela["retorno"];
            $this->limite = $tabela["limite"];
            $this->registros = $tabela["total"];
            $this->paginas = self::paginacao($this->p, $tabela["paginas"]);
        }
    }

    public function cadastrar() {
        if (isset($_POST['submit'])) {
            $this->extract($_POST);
            $action = dao_plano::cadastrar($this->nome);
            if ($action) {
                $this->session_growl("Cadastro de plano", "Plano cadastrado com sucesso!");
                knife::redirect("index.html?a=plano/index");
            }
        }
    }

    public function alterar() {
        if ($this->id) {
            if (isset($_POST['submit'])) {
                $this->extract($_POST);
                $action = dao_plano::atualizar($this->id, $this->nome);
                if ($action) {
                    $this->session_growl("Alteração de plano", "Plano alterado com sucesso!");
                    knife::redirect("index.html?a=plano/index");
                }
            }
            $this->abrir($this->id);
        } else {
            knife::redirect("index.html?a=plano/cadastrar");
        }
    }

    /**
     * Remove Item
     * @param integer $id
     */
    public function remover() {
        $this->session_growl("Remover plano", "Nenhum plano selecionado!", 'error');
        if ($_POST) {
            if (!empty($_POST['id'])) {
                $removidos = true;
                foreach ($_POST['id'] as $id) {
                    if (!dao_plano::remover($id)) {
                        $removidos = false;
                    }
                }
                $this->session_growl("Remover plano", ($removidos ? "Plano(s) removido com sucesso!" : "Alguns plano(s) não foram removidos!"), ($removidos ? 'success' : 'warn'));
            }
        }
        knife::redirect("index.html?a=plano/index");
    }

    /**
     * Carrega Item
     * @param integer $id
     * @return boolean
     */
    private function abrir($id) {
        $dao = (dao_plano::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

}