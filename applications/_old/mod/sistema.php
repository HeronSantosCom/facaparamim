<?php

class mod_sistema extends main {

    public function __construct() {
        $this->extract($_GET);
    }

    public function index() {
        $this->extract(self::filtro("sistema/index"));
        $tabela = dao_sistema::tabela(array("nome" => $this->nome), $this->p);
        if ($tabela) {
            $this->sistemas = $tabela["retorno"];
            $this->limite = $tabela["limite"];
            $this->registros = $tabela["total"];
            $this->paginas = self::paginacao($this->p, $tabela["paginas"]);
        }
    }

    public function cadastrar() {
       if (isset($_POST['submit'])) {
            $this->extract($_POST);
            $action = ($this->id ? dao_sistema::atualizar($this->id, $this->nome) : dao_sistema::cadastrar($this->nome));
            if ($action) {
                $this->session_growl("Cadastro de sistema", "Sistema cadastrado com sucesso!");
                knife::redirect("index.html?a=sistema/index");
            }
        }
    }

    public function alterar() {
        if ($this->id) {
            if (isset($_POST['submit'])) {
                $this->extract($_POST);
                $action = dao_sistema::atualizar($this->id, $this->nome);
                if ($action) {
                    $this->session_growl("Alteração de sistema", "Sistema alterado com sucesso!");
                    knife::redirect("index.html?a=sistema/index");
                }
                return;
            }
            $this->abrir($this->id);
        } else {
            knife::redirect("index.html?a=modulo/cadastrar");
        }
    }

    /**
     * Remove Item
     * @param integer $id
     */
    public function remover() {
        $this->session_growl("Remover sistema", "Nenhum sistema selecionado!", 'error');
        if ($_POST) {
            if (!empty($_POST['id'])) {
                $removidos = true;
                foreach ($_POST['id'] as $id) {
                    if (!dao_sistema::remover($id)) {
                        $removidos = false;
                    }
                }
                $this->session_growl("Remover sistema", ($removidos ? "Sistema(s) removido com sucesso!" : "Alguns sistema(s) não foram removidos!"), ($removidos ? 'success' : 'warn'));
            }
        }
        knife::redirect("index.html?a=sistema/index");
    }

    /**
     * Carrega Item
     * @param integer $id
     * @return boolean
     */
    private function abrir($id) {
        $dao = (dao_sistema::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

}