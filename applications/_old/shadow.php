<?php

class shadow {

    public static function set($string) {
//        $md5 = new md5();
//        return $md5->set(array(date("d/m/Y"), $string)), '+/=', '-_.');
        return strtr(base64_encode(serialize($string)), '+/=', '-_.');
    }

    public static function get($hash) {
//        $md5 = new md5();
//        $hash = $md5->get(strtr($hash, '-_.', '+/='));
//        return (!empty($hash[1]) ? $hash[1] : false);
        return unserialize(base64_decode(strtr($hash, '-_.', '+/=')));
    }

}