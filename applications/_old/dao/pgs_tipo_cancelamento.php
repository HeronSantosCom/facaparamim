<?php

class dao_pgs_tipo_cancelamento {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("pgs_tipo_cancelamento");
        return $db;
    }

    private static function hook($row) {
        return $row;
    }

    public static function pegar($codigo, $id = false) {
        $db = self::dao();
        $db->column("*", false);
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->match("codigo", $codigo);
        }
        $dao = $db->go();
        if ($dao[0]) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $db = self::dao();
        $db->column("*", false);
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

}