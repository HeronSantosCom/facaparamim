<?php

class dao_crp_transacao {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("crp_transacao"); // 1
        $db->join("usuario", array("usuario_id", "=", "id", 1), "LEFT"); // 2
        $db->join("crp_tipo_transacao", array("crp_tipo_transacao_id", "=", "id", 1), "LEFT"); // 3
        return $db;
    }

    private static function hook($row) {
        $row["gravatar"] = knife::gravatar($row["usuario_email"], 360);
        $row["valor_br"] = number_format($row["valor"], 5, ',', '.');
        $row["valor_br2"] = number_format($row["valor"], 2, ',', '.');
        $row["cadastrado_br"] = date("d/m/Y", strtotime($row["cadastrado"]));
        $row["cadastrado_br2"] = date("d/m", strtotime($row["cadastrado"]));
        $row["efetivado_br"] = ($row["efetivado"] ? date("d/m/Y", strtotime($row["efetivado"])) : false);
        $row["efetivado"] = ($row["efetivado"] ? '1' : '0');
        if (!empty($row["total"])) {
            $row["total_br"] = number_format($row["total"], 5, ',', '.');
            $row["total_br2"] = number_format($row["total"], 2, ',', '.');
        }
        return $row;
    }

    public static function pegar($id, $codigo = false) {
        $db = self::dao();
        $db->column("*", false);
        if ($id) {
            $db->match("id", $id);
        }
        if ($codigo) {
            $db->match("codigo", $codigo);
        }
        $dao = $db->go();
        if ($dao[0]) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $efetivado = false, $crp_tipo_transacao_id = false, $cadastrado = false, $agrupado = false) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("usuario_id", $usuario_id);
        if ($efetivado) {
            if (is_array($cadastrado)) {
                $db->between("DATE(efetivado)", $efetivado);
            } else {
                $db->match("DATE(efetivado)", $efetivado);
            }
        }
        if ($crp_tipo_transacao_id) {
            if (is_array($crp_tipo_transacao_id)) {
                $db->in("crp_tipo_transacao_id", $crp_tipo_transacao_id);
            } else {
                $db->match("crp_tipo_transacao_id", $crp_tipo_transacao_id);
            }
        }
        if ($cadastrado) {
            if (is_array($cadastrado)) {
                $db->between("DATE(cadastrado)", $cadastrado);
            } else {
                $db->match("DATE(cadastrado)", $cadastrado);
            }
        }
        if ($agrupado) {
            $db->column("MONTH(cadastrado)", false, "mes");
            $db->column("YEAR(cadastrado)", false, "ano");
            $db->column("SUM(valor)", false, "total");
            $db->column("COUNT(id)", false, "qtd");
            $db->group("mes");
            $db->group("ano");
            $db->group("crp_tipo_transacao_id");
            $db->group("descricao");
            $db->group("valor");
            $db->order("ano");
            $db->order("mes");
            $db->order("crp_tipo_transacao_id");
        } else {
            $db->order("cadastrado");
        }
        $db->is("cancelado", false);
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    public static function cadastrar($pid, $codigo, $descricao, $valor, $cadastrado, $efetivado, $crp_tipo_transacao_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("pid", $pid);
        $db->column("codigo", $codigo);
        $db->column("descricao", $descricao);
        $db->column("valor", $valor);
        $db->column("cadastrado", $cadastrado);
        $db->column("efetivado", $efetivado);
        $db->column("crp_tipo_transacao_id", $crp_tipo_transacao_id);
        $db->column("usuario_id", $usuario_id);
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function cancelar($id, $codigo = false) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("cancelado", date("Y-m-d H:i:s"));
        if ($id) {
            $db->match("id", $id);
        }
        if ($codigo) {
            $db->match("codigo", $codigo);
        }
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function efetivar($id, $codigo = false) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("efetivado", date("Y-m-d H:i:s"));
        if ($id) {
            $db->match("id", $id);
        }
        if ($codigo) {
            $db->match("codigo", $codigo);
        }
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function gerar_codigo($usuario_id) {
        return substr(sha1(uniqid(date("r") . $usuario_id . mt_rand(), true)), 0, 8);
    }

}