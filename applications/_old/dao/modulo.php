<?php

class dao_modulo {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("modulo"); // 1
        return $db;
    }

    private static function hook($row) {
        $row["id"] = shadow::set($row["id"]);
        $row["modulo_id"] = shadow::set($row["modulo_id"]);
        return $row;
    }

    public static function tabela($filtro = false, $pagina = 1, $limite = 20) {
        $db = self::dao();
        $db->column("COUNT(id)", false, "total");
        if ($filtro) {
            foreach ($filtro as $key => $value) {
                switch ($key) {
                    case "id":
                        $db->match("id", shadow::get($value));
                        break;
                    case "modulo_id":
                        $db->match("modulo_id", shadow::get($value));
                        break;
                    default:
                        $db->like($key, "%{$value}%");
                        break;
                }
            }
        }
        $where = $db->where;
        $dao = $db->go();
        $total = ($dao ? $dao[0]["total"] : 0);
        $pagina = ($pagina ? $pagina : 1);
        $paginas = ceil($total > 0 ? $total / $limite : 1);
        $inicial = ($pagina * $limite) - $limite;
        return array("total" => $total, "limite" => $limite, "paginas" => $paginas, "retorno" => self::listar(array($limite, $inicial, $where)));
    }

    public static function pegar($id, $caminho = false) {
        $db = self::dao();
        $db->column("*", false);
        if ($id) {
            $db->match("id", shadow::get($id));
        } else {
            if ($caminho) {
                $db->match("caminho", $caminho);
            }
        }
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($filtro = false, $modulo_id = false) {
        $db = self::dao();
        $db->column("*", false);
        if (is_array($filtro)) {
            $db->where = $filtro[2];
            $db->limit($filtro[0], $filtro[1]);
        } else {
            if ($modulo_id) {
                $db->match("modulo_id", shadow::get($modulo_id));
            }
        }
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    private static function verificar($nome, $id = false) {
        $db = new mysqlsearch();
        $db->table("modulo");
        $db->column("id");
        $db->match("nome", $nome);
        if ($id) {
            $db->match("id", shadow::get($id), false, true);
        }
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function cadastrar($nome, $caminho, $modal, $navbar, $navuser, $modulo_id) {
        if (!self::verificar($nome)) {
            $db = new mysqlsave();
            $db->table("modulo");
            $db->column("nome", $nome);
            $db->column("caminho", $caminho);
            $db->column("modal", $modal);
            $db->column("navbar", $navbar);
            $db->column("navuser", $navuser);
            $db->column("modulo_id", (!empty($modulo_id) ? shadow::get($modulo_id) : null));
            if ($db->go()) {
                return shadow::set($db->id());
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $caminho, $modal, $navbar, $navuser, $modulo_id) {
        if (!self::verificar($nome, $id)) {
            $db = new mysqlsave();
            $db->table("modulo");
            $db->column("nome", $nome);
            $db->column("caminho", $caminho);
            $db->column("modal", $modal);
            $db->column("navbar", $navbar);
            $db->column("navuser", $navuser);
            $db->column("modulo_id", shadow::get($modulo_id));
            $db->match("id", shadow::get($id));
            if ($db->go()) {
                return true;
            }
        }
        return false;
    }

    public static function remover($id) {
        $db = new mysqldelete();
        $db->table("modulo");
        $db->match("id", shadow::get($id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

}
