<?php

class dao_pgs_tipo_meio_pagamento {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("pgs_tipo_meio_pagamento");
        return $db;
    }

    private static function hook($row) {
        return $row;
    }

    public static function pegar($id) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $db = self::dao();
        $db->column("*", false);
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

}