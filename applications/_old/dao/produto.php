<?php

class dao_produto {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("produto"); // 1
        $db->join("plano", array("plano_id", "=", "id", 1), "LEFT"); // 2
        $db->join("sistema", array("sistema_id", "=", "id", 1), "LEFT"); // 3
        return $db;
    }

    private static function hook($row) {
        $row["id"] = shadow::set($row["id"]);
        $row["plano_id"] = shadow::set($row["plano_id"]);
        $row["sistema_id"] = shadow::set($row["sistema_id"]);
        $row["gravatar"] = knife::gravatar($row["nome"], 360);
        return $row;
    }

    public static function pegar($id) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("id", shadow::get($id));
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function tabela($filtro = false, $pagina = 1, $limite = 20) {
        $db = self::dao();
        $db->column("COUNT(id)", false, "total");
        if ($filtro) {
            foreach ($filtro as $key => $value) {
                switch ($key) {
                    case "id":
                        $db->match("id", shadow::get($value));
                        break;
                    case "sistema_id":
                        $db->match("sistema_id", shadow::get($value));
                        break;
                    case "plano_id":
                        $db->match("plano_id", shadow::get($value));
                        break;
                    default:
                        $db->like($key, "%{$value}%");
                        break;
                }
            }
        }
        $db->is("removido", false);
        $where = $db->where;
        $dao = $db->go();
        $total = ($dao ? $dao[0]["total"] : 0);
        $pagina = ($pagina ? $pagina : 1);
        $paginas = ceil($total > 0 ? $total / $limite : 1);
        $inicial = ($pagina * $limite) - $limite;
        return array("total" => $total, "limite" => $limite, "paginas" => $paginas, "retorno" => self::listar(array($limite, $inicial, $where)));
    }

    public static function listar($filtro = false) {
        $db = self::dao();
        $db->column("*", false);
        if (is_array($filtro)) {
            $db->where = $filtro[2];
            $db->limit($filtro[0], $filtro[1]);
        } else {
            $db->is("removido", false);
        }
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    private static function verificar($nome, $id = false) {
        $db = new mysqlsearch();
        $db->table("produto");
        $db->column("id");
        $db->match("nome", $nome);
        if ($id) {
            $db->match("id", shadow::get($id), false, true);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function cadastrar($nome, $plano_id, $sistema_id) {
        if (!self::verificar($nome)) {
            $db = new mysqlsave();
            $db->table("produto");
            $db->column("nome", $nome);
            $db->column("cadastrado", date("Y-m-d H:i:s"));
            $db->column("atualizado", date("Y-m-d H:i:s"));
            $db->column("plano_id", shadow::get($plano_id));
            $db->column("sistema_id", shadow::get($sistema_id));
            if ($db->go()) {
                return shadow::set($db->id());
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $plano_id, $sistema_id) {
        if (!self::verificar($nome, $id)) {
            $db = new mysqlsave();
            $db->table("produto");
            $db->column("nome", $nome);
            $db->column("atualizado", date("Y-m-d H:i:s"));
            $db->column("plano_id", shadow::get($plano_id));
            $db->column("sistema_id", shadow::get($sistema_id));
            $db->match("id", $id);
            if ($db->go()) {
                return true;
            }
        }
        return false;
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("produto");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("id", shadow::get($id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

}
