<?php

class dao_crp_tipo_transacao {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("crp_tipo_transacao");
        return $db;
    }

    private static function hook($row) {
        return $row;
    }

    public static function pegar($id) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("id", $id);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $db = self::dao();
        $db->column("*", false);
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

}
