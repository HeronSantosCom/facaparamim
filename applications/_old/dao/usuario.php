<?php

class dao_usuario {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("usuario"); // 1
        return $db;
    }

    private static function hook($row) {
        $row["id"] = shadow::set($row["id"]);
        $row["gravatar"] = knife::gravatar($row["email"], 360);
        return $row;
    }

    public static function pegar($id, $usuario = false, $senha = false) {
        $db = self::dao();
        $db->column("*", false);
        if ($id) {
            $db->match("id", shadow::get($id));
        } else {
            $db->match("email", $usuario);
            if ($senha) {
                $db->match("senha", md5($senha));
            }
        }
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function tabela($filtro = false, $id = false, $pagina = 1, $limite = 20) {
        $db = self::dao();
        $db->column("COUNT(id)", false, "total");
        if ($filtro) {
            foreach ($filtro as $key => $value) {
                switch ($key) {
                    case "id":
                        $db->match("id", shadow::get($value));
                        break;
                    default:
                        $db->like($key, "%{$value}%");
                        break;
                }
            }
        }
        $db->match("id", shadow::get($id), false, true);
        $db->is("removido", false);
        $where = $db->where;
        $dao = $db->go();
        $total = ($dao ? $dao[0]["total"] : 0);
        $pagina = ($pagina ? $pagina : 1);
        $paginas = ceil($total > 0 ? $total / $limite : 1);
        $inicial = ($pagina * $limite) - $limite;
        return array("total" => $total, "limite" => $limite, "paginas" => $paginas, "retorno" => self::listar(array($limite, $inicial, $where)));
    }

    public static function listar($filtro = false, $id = false) {
        $db = self::dao();
        $db->column("*", false);
        if (is_array($filtro)) {
            $db->where = $filtro[2];
            $db->limit($filtro[0], $filtro[1]);
        } else {
            $db->match("id", shadow::get($id), false, true);
            $db->is("removido", false);
        }
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    private static function verificar($email, $id = false) {
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("id");
        $db->match("email", $email);
        if ($id) {
            $db->match("id", shadow::get($id), false, true);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0]) && !knife::is_mail($email)) {
            return true;
        }
        return false;
    }

    public static function cadastrar($nome, $apelido, $email, $telefone, $senha) {
        if (!self::verificar($email)) {
            $db = new mysqlsave();
            $db->table("usuario");
            $db->column("nome", $nome);
            $db->column("apelido", $apelido);
            $db->column("email", $email);
            $db->column("telefone", $telefone);
            $db->column("senha", $senha);
            $db->column("cadastrado", date("Y-m-d H:i:s"));
            $db->column("atualizado", date("Y-m-d H:i:s"));
            if ($db->go()) {
                return shadow::set($db->id());
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $apelido, $email, $telefone) {
        if (!self::verificar($email, $id)) {
            $db = new mysqlsave();
            $db->table("usuario");
            $db->column("nome", $nome);
            $db->column("apelido", $apelido);
            $db->column("telefone", $telefone);
            $db->column("atualizado", date("Y-m-d H:i:s"));
            $db->match("id", shadow::get($id));
            if ($db->go()) {
                return true;
            }
        }
        return false;
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("id", shadow::get($id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function redefinir_senha($email, $senha) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("senha", md5($senha));
        $db->match("email", $email);
        $db->is("removido", false);
        if ($db->go()) {
            return true;
        }
        return false;
    }

}
