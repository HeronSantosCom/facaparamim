<?php

class dao_perfil_acesso_x_modulo {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("perfil_acesso_x_modulo"); // 1
        $db->join("perfil_acesso", array("perfil_acesso_id", "=", "id", 1), "LEFT"); // 2
        $db->join("modulo", array("modulo_id", "=", "id", 1), "LEFT"); // 3
        return $db;
    }

    private static function hook($row) {
        $row["perfil_acesso_id"] = shadow::set($row["perfil_acesso_id"]);
        $row["modulo_id"] = shadow::set($row["modulo_id"]);
        return $row;
    }

    public static function pegar($perfil_acesso_id, $modulo_id = false, $caminho = false) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("perfil_acesso_id", shadow::get($perfil_acesso_id));
        if ($caminho) {
            $db->match("caminho", $caminho, false, false, 3);
        } else {
            $db->match("modulo_id", shadow::get($modulo_id));
        }
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function tabela($filtro = false, $pagina = 1, $limite = 20) {
        $db = self::dao();
        $db->column("COUNT(id)", false, "total");
        if ($filtro) {
            foreach ($filtro as $key => $value) {
                switch ($key) {
                    case "perfil_acesso_id":
                        $db->match("perfil_acesso_id", shadow::get($value));
                        break;
                    case "modulo_id":
                        $db->match("modulo_id", shadow::get($value));
                        break;
                    default:
                        $db->like($key, "%{$value}%");
                        break;
                }
            }
        }
        $where = $db->where;
        $dao = $db->go();
        $total = ($dao ? $dao[0]["total"] : 0);
        $pagina = ($pagina ? $pagina : 1);
        $paginas = ceil($total > 0 ? $total / $limite : 1);
        $inicial = ($pagina * $limite) - $limite;
        return array("total" => $total, "limite" => $limite, "paginas" => $paginas, "retorno" => self::listar(array($limite, $inicial, $where)));
    }

    public static function listar($filtro = false, $perfil_acesso_id = false, $modulo_id = false) {
        $db = self::dao();
        $db->column("*", false);
        if (is_array($filtro)) {
            $db->where = $filtro[2];
            $db->limit($filtro[0], $filtro[1]);
        } else {
            if ($perfil_acesso_id) {
                $db->match("perfil_acesso_id", shadow::get($perfil_acesso_id));
            }
            if ($modulo_id) {
                $db->match("modulo_id", shadow::get($modulo_id));
            }
        }
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    public static function cadastrar($perfil_acesso_id, $modulo_id) {
        $db = new mysqlsave();
        $db->table("perfil_acesso_x_modulo");
        $db->column("perfil_acesso_id", shadow::get($perfil_acesso_id));
        $db->column("modulo_id", shadow::get($modulo_id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function remover($perfil_acesso_id, $modulo_id) {
        $db = new mysqldelete();
        $db->table("perfil_acesso_x_modulo");
        $db->match("perfil_acesso_id", shadow::get($perfil_acesso_id));
        $db->match("modulo_id", shadow::get($modulo_id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

}