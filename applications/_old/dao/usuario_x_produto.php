<?php

class dao_usuario_x_produto {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("usuario_x_produto"); // 1
        $db->join("usuario", array("usuario_id", "=", "id", 1), "INNER"); // 2
        $db->join("produto", array("produto_id", "=", "id", 1), "INNER"); // 3
        $db->join("perfil_acesso", array("perfil_acesso_id", "=", "id", 1), "LEFT"); // 4
        return $db;
    }

    private static function hook($row) {
        $row["usuario_id"] = shadow::set($row["usuario_id"]);
        $row["produto_id"] = shadow::set($row["produto_id"]);
        $row["perfil_acesso_id"] = shadow::set($row["perfil_acesso_id"]);
        $row["gravatar"] = knife::gravatar($row["usuario_email"], 360);
        return $row;
    }

    public static function pegar($usuario_id, $produto_id) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("usuario_id", shadow::get($usuario_id));
        $db->match("produto_id", shadow::get($produto_id));
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function tabela($filtro = false, $pagina = 1, $limite = 20) {
        $db = self::dao();
        $db->column("COUNT(id)", false, "total");
        if ($filtro) {
            foreach ($filtro as $key => $value) {
                switch ($key) {
                    case "usuario_id":
                        $db->match("usuario_id", shadow::get($value));
                        break;
                    case "produto_id":
                        $db->match("produto_id", shadow::get($value));
                        break;
                    case "perfil_acesso_id":
                        $db->match("perfil_acesso_id", shadow::get($value));
                        break;
                    default:
                        $db->like($key, "%{$value}%");
                        break;
                }
            }
        }
        $where = $db->where;
        $dao = $db->go();
        $total = ($dao ? $dao[0]["total"] : 0);
        $pagina = ($pagina ? $pagina : 1);
        $paginas = ceil($total > 0 ? $total / $limite : 1);
        $inicial = ($pagina * $limite) - $limite;
        return array("total" => $total, "limite" => $limite, "paginas" => $paginas, "retorno" => self::listar(array($limite, $inicial, $where)));
    }

    public static function listar($filtro = false, $usuario_id = false, $produto_id = false) {
        $db = self::dao();
        $db->column("*", false);
        if (is_array($filtro)) {
            $db->where = $filtro[2];
            $db->limit($filtro[0], $filtro[1]);
        } else {
            if ($usuario_id) {
                $db->match("usuario_id", shadow::get($usuario_id));
            }
            if ($produto_id) {
                $db->match("produto_id", shadow::get($produto_id));
            }
        }
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    public static function cadastrar($usuario_id, $produto_id, $perfil_acesso_id) {
        $db = new mysqlsave();
        $db->table("usuario_x_produto");
        $db->column("usuario_id", shadow::get($usuario_id));
        $db->column("produto_id", shadow::get($produto_id));
        $db->column("perfil_acesso_id", shadow::get($perfil_acesso_id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function remover($usuario_id, $produto_id) {
        $db = new mysqldelete();
        $db->table("usuario_x_produto");
        $db->match("usuario_id", shadow::get($usuario_id));
        $db->match("produto_id", shadow::get($produto_id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

}