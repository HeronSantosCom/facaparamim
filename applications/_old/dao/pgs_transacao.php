<?php

class dao_pgs_transacao {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("pgs_transacao");
        $db->join("crp_transacao", array("crp_transacao_id", "=", "id", 1), "LEFT");
        $db->join("pgs_tipo_transacao", array("pgs_tipo_transacao_id", "=", "id", 1), "LEFT");
        $db->join("pgs_tipo_status", array("pgs_tipo_status_id", "=", "id", 1), "LEFT");
        $db->join("pgs_tipo_meio_pagamento", array("pgs_tipo_meio_pagamento_id", "=", "id", 1), "LEFT");
        $db->join("pgs_tipo_cancelamento", array("pgs_tipo_cancelamento_id", "=", "id", 1), "LEFT");
        return $db;
    }

    private static function hook($row) {
        $row["cadastrado_br"] = date("d/m/Y H:i", strtotime($row["cadastrado"]));
        return $row;
    }

    public static function listar($crp_transacao_id) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("crp_transacao_id", $crp_transacao_id);
        $db->order("cadastrado", "ASC");
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    public static function cadastrar($codigo, $parcelas, $pgs_tipo_cancelamento_id, $pgs_tipo_status_id, $pgs_tipo_transacao_id, $pgs_tipo_meio_pagamento_id, $crp_transacao_id) {
        $db = new mysqlsave();
        $db->table("pgs_transacao");
        $db->column("codigo", $codigo);
        $db->column("parcelas", $parcelas);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        $db->column("pgs_tipo_cancelamento_id", $pgs_tipo_cancelamento_id);
        $db->column("pgs_tipo_status_id", $pgs_tipo_status_id);
        $db->column("pgs_tipo_transacao_id", $pgs_tipo_transacao_id);
        $db->column("pgs_tipo_meio_pagamento_id", $pgs_tipo_meio_pagamento_id);
        $db->column("crp_transacao_id", $crp_transacao_id);
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

}