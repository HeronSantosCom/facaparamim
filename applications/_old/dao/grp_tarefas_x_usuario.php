<?php

class dao_grp_tarefas_x_usuario {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("grp_tarefas_x_usuario"); // 1
        $db->join("grp_tarefas", array("grp_tarefas_id", "=", "id", 1), "LEFT"); // 2
        $db->join("usuario", array("usuario_id", "=", "id", 1), "LEFT"); // 3
        return $db;
    }

    private static function hook($row) {
        $row["grp_tarefas_id"] = shadow::set($row["grp_tarefas_id"]);
        $row["usuario_id"] = shadow::set($row["usuario_id"]);
        $row["gravatar"] = knife::gravatar($row["usuario_email"], 360);
        return $row;
    }

    public static function pegar($grp_tarefas_id, $usuario_id = false) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("grp_tarefas_id", shadow::get($grp_tarefas_id));
        $db->match("usuario_id", shadow::get($usuario_id));
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function tabela($filtro = false, $pagina = 1, $limite = 20) {
        $db = self::dao();
        $db->column("COUNT(id)", false, "total");
        if ($filtro) {
            foreach ($filtro as $key => $value) {
                switch ($key) {
                    case "grp_tarefas_id":
                        $db->match("grp_tarefas_id", shadow::get($value));
                        break;
                    case "usuario_id":
                        $db->match("usuario_id", shadow::get($value));
                        break;
                    default:
                        $db->like($key, "%{$value}%");
                        break;
                }
            }
        }
        $where = $db->where;
        $dao = $db->go();
        $total = ($dao ? $dao[0]["total"] : 0);
        $pagina = ($pagina ? $pagina : 1);
        $paginas = ceil($total > 0 ? $total / $limite : 1);
        $inicial = ($pagina * $limite) - $limite;
        return array("total" => $total, "limite" => $limite, "paginas" => $paginas, "retorno" => self::listar(array($limite, $inicial, $where)));
    }

    public static function listar($filtro = false, $grp_tarefas_id = false, $usuario_id = false) {
        $db = self::dao();
        $db->column("*", false);
        if (is_array($filtro)) {
            $db->where = $filtro[2];
            $db->limit($filtro[0], $filtro[1]);
        } else {
            if ($grp_tarefas_id) {
                $db->match("grp_tarefas_id", shadow::get($grp_tarefas_id));
            }
            if ($usuario_id) {
                $db->match("usuario_id", shadow::get($usuario_id));
            }
        }
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    public static function cadastrar($grp_tarefas_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("grp_tarefas_x_usuario");
        $db->column("grp_tarefas_id", shadow::get($grp_tarefas_id));
        $db->column("usuario_id", shadow::get($usuario_id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function remover($grp_tarefas_id, $usuario_id) {
        $db = new mysqldelete();
        $db->table("grp_tarefas_x_usuario");
        $db->match("grp_tarefas_id", shadow::get($grp_tarefas_id));
        $db->match("usuario_id", shadow::get($usuario_id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

}