<?php

class dao_grp_tarefas {

    private static function dao() {
        $db = new mysqlsearch();
        $db->table("grp_tarefas"); // 1
        $db->join("grp_tarefas", array("grp_tarefas_id", "=", "id", 1), "LEFT"); // 2
        $db->join("usuario", array("usuario_id", "=", "id", 1), "LEFT"); // 3
        return $db;
    }

    private static function hook($row) {
        $row["id"] = shadow::set($row["id"]);
        $row["grp_tarefas_id"] = shadow::set($row["grp_tarefas_id"]);
        $row["usuario_id"] = shadow::set($row["usuario_id"]);
        $row["gravatar"] = knife::gravatar($row["usuario_email"], 360);
        return $row;
    }

    public static function pegar($id) {
        $db = self::dao();
        $db->column("*", false);
        $db->match("id", shadow::get($id));
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function tabela($filtro = false, $pagina = 1, $limite = 20) {
        $db = self::dao();
        $db->column("COUNT(id)", false, "total");
        if ($filtro) {
            foreach ($filtro as $key => $value) {
                switch ($key) {
                    case "id":
                        $db->match("id", shadow::get($value));
                        break;
                    case "usuario_id":
                        $db->match("usuario_id", shadow::get($value));
                        break;
                    case "grp_tarefas_id":
                        $db->match("grp_tarefas_id", shadow::get($value));
                        break;
                    default:
                        $db->like($key, "%{$value}%");
                        break;
                }
            }
        }
        $where = $db->where;
        $dao = $db->go();
        $total = ($dao ? $dao[0]["total"] : 0);
        $pagina = ($pagina ? $pagina : 1);
        $paginas = ceil($total > 0 ? $total / $limite : 1);
        $inicial = ($pagina * $limite) - $limite;
        return array("total" => $total, "limite" => $limite, "paginas" => $paginas, "retorno" => self::listar(array($limite, $inicial, $where)));
    }

    public static function listar($filtro = false) {
        $db = self::dao();
        $db->column("*", false);
        if (is_array($filtro)) {
            $db->where = $filtro[2];
            $db->limit($filtro[0], $filtro[1]);
        }
        $dao = $db->go();
        if ($dao) {
            $array = false;
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
            return $array;
        }
        return false;
    }

    private static function verificar($nome, $id = false) {
        $db = new mysqlsearch();
        $db->table("grp_tarefas");
        $db->column("id");
        $db->match("nome", $nome);
        if ($id) {
            $db->match("id", shadow::get($id), false, true);
        }
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function cadastrar($nome, $grp_tarefas_id, $usuario_id, $data_inicio, $previsao, $status) {
        if (!self::verificar($nome)) {
            $db = new mysqlsave();
            $db->table("grp_tarefas");
            $db->column("nome", $nome);
            $db->column("grp_tarefas_id", shadow::get($grp_tarefas_id));
            $db->column("usuario_id", shadow::get($usuario_id));
            $db->column("data_inicio", $data_inicio);
            $db->column("previsao", $previsao);
            $db->column("status", $status);
            if ($db->go()) {
                return shadow::set($db->id());
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $grp_tarefas_id, $usuario_id, $data_inicio, $previsao, $status) {
        if (!self::verificar($nome, $id)) {
            $db = new mysqlsave();
            $db->table("grp_tarefas");
            $db->column("nome", $nome);
            $db->column("grp_tarefas_id", shadow::get($grp_tarefas_id));
            $db->column("usuario_id", shadow::get($usuario_id));
            $db->column("data_inicio", $data_inicio);
            $db->column("previsao", $previsao);
            $db->column("status", $status);
            $db->match("id", $id);
            if ($db->go()) {
                return true;
            }
        }
        return false;
    }

    public static function remover($id) {
        $db = new mysqldelete();
        $db->table("grp_tarefas");
        $db->match("id", shadow::get($id));
        if ($db->go()) {
            return true;
        }
        return false;
    }

}
