<?php

class main extends logon {

    public function __construct() {
        $this->autoloader();
        // carrega logon
        parent::__construct();
        // redefine o titulo
        $this->titulo(name);
        $this->session_msgbox();
        $this->session_growl();
        //verifica se não está logado, abre as determinadas páginas
        if (!parent::meu_id()) {
            switch ($_SERVER["REDIRECT_URL"]) {
                case "/entrar.html":
                    return true;
                    break;
                case "/tpl.html":
                    return true;
                    break;
                case "/redefinir.html":
                    return $this->redefinir_senha();
                    break;
                default:
                    knife::redirect("entrar.html");
                    break;
            }
        }
        // verifica se está chamando o módulo beta
        if (isset($_GET["beta"])) {
            new beta();
            die();
        }
        // se não for ajax nem index, retorna ao index senão carrega
        switch ($_SERVER["REDIRECT_URL"]) {
            case "/index.html":
            case "/xhr.html":
                return $this->modulo();
                break;
            default:
                knife::redirect("/index.html");
                break;
        }
    }

    private function autoloader() {
        if (function_exists('__autoload')) {
            spl_autoload_register('__autoload');
        }
        spl_autoload_register(array($this, 'autoclasse'));
    }

    protected static function autoclasse($classe) {
        $classe = explode("_", $classe, 2);
        if (!empty($classe[1])) {
            if (!class_exists("{$classe[0]}_{$classe[1]}", false) && file_exists(knife::application("{$classe[0]}/{$classe[1]}.php"))) {
                include knife::application("{$classe[0]}/{$classe[1]}.php");
            }
        }
    }

    private static function is_xhr() {
        return ($_SERVER["REDIRECT_URL"] == "/xhr.html");
    }

    protected static function paginacao($atual = 1, $total = 20) {
        $atual = ($atual ? $atual : 1);
        if ($atual > 1) {
            $arr[] = array(
                "id" => ($atual - 1),
                "nome" => "Anterior",
                "ativo" => false
            );
        }
        for ($pg = 1; $pg <= $total; $pg++) {
            $arr[] = array(
                "id" => $pg,
                "nome" => $pg,
                "ativo" => ($atual == $pg)
            );
        }
        if ($atual < $total) {
            $arr[] = array(
                "id" => ($atual + 1),
                "nome" => "Proximo",
                "ativo" => false
            );
        }
        return (!empty($arr) ? $arr : false);
    }

    protected static function filtro($modulo) {
        $arr = explode("/", $modulo, 2);
        if (!empty($arr[0])) {
            if (isset($_POST["filtrar"])) {
                $_SESSION["filtro"][$arr[0]] = ($_POST["filtrar"] ? $_POST : false);
                knife::redirect("index.html?a={$modulo}");
            } else {
                $repassar = (!empty($_SESSION["filtro"][$arr[0]]) ? $_SESSION["filtro"][$arr[0]] : false);
                unset($_SESSION["filtro"]);
                $_SESSION["filtro"][$arr[0]] = $repassar;
                return $repassar;
            }
        }
        return false;
    }

    protected function msgbox($msgbox) {
        $this->msgbox = $msgbox;
    }

    protected function session_msgbox($msgbox = false) {
        if ($msgbox) {
            $_SESSION["msgbox"] = $msgbox;
        } else {
            if (!empty($_SESSION["msgbox"])) {
                $this->msgbox($_SESSION["msgbox"]);
                unset($_SESSION["msgbox"]);
            }
        }
    }

    protected function growl($title = false, $message = false, $type = 'success') {
        $this->growl_title = $title;
        $this->growl_message = $message;
        $this->growl_type = $type;
    }

    protected function session_growl($title = false, $message = false, $type = 'success') {
        if ($message) {
            $_SESSION["growl"]['title'] = $title;
            $_SESSION["growl"]['message'] = $message;
            $_SESSION["growl"]['type'] = $type;
        } else {
            if (!empty($_SESSION["growl"])) {
                $this->growl($_SESSION["growl"]['title'], $_SESSION["growl"]['message'], $_SESSION["growl"]['type']);
                unset($_SESSION["growl"]);
            }
        }
    }

    protected function nome($nome) {
        $this->layout_nome = $nome;
        $this->pagina_titulo .= " > {$nome}";
    }

    protected function titulo($nome) {
        $this->pagina_titulo = $nome;
    }

    protected function app($aplicacao) {
        $aplicacao = explode("/", $aplicacao);
        $funcao = (!empty($aplicacao[1]) ? $aplicacao[1] : false);
        if (!class_exists("mod_{$aplicacao[0]}", false) and file_exists(knife::application("mod/{$aplicacao[0]}.php"))) {
            $aplicacao = "mod_{$aplicacao[0]}";
            $instance = new $aplicacao();
            if ($funcao) {
                $instance->{$funcao}();
            }
            if (defined("app_module_error")) {
                return false;
            }
        }
        return true;
    }

    protected function css($css) {
        if (file_exists(knife::source($css))) {
            $this->layout_css = $css;
            return true;
        }
        return false;
    }

    protected function html($html) {
        if (file_exists(knife::source($html))) {
            $this->layout_html = knife::html($html);
            return true;
        }
        return false;
    }

    private function modulo() {
        $caminho = (!empty($_GET["a"]) ? $_GET["a"] : "index");
        $modulo = dao_modulo::pegar(false, $caminho);
//        $modulo = dao_perfil_acesso_x_modulo::pegar(logon::meu_perfil_acesso_id(), false, $caminho);
        if ($modulo) {
            $this->layout_caminho = $caminho;
            $this->nome($modulo["nome"]);
            if (self::is_xhr()) {
                return $this->app($modulo["caminho"]);
            } else {
                if (file_exists(knife::source("layout/main/{$modulo["caminho"]}.html"))) {
                    if ($this->app($modulo["caminho"])) {
                        $this->css("css/pages/{$modulo["caminho"]}.css");
                        return $this->html("layout/main/{$modulo["caminho"]}.html");
                    }
                }
            }
        }
        return $this->html("layout/main/erro.html");
    }

}